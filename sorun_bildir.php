
<?php

ob_start();
include 'baglanti.php';

include 'header.php';

?>
<style>
.jumbotron {
background: #358CCE;
color: #FFF;
border-radius: 0px;
}
.jumbotron-sm { padding-top: 30px;
padding-bottom: 30px; }
.jumbotron small {
color: #FFF;
}
.h1 small {
font-size: 30px;
}

</style>
<link href="//netdna.bootstrapcdn.com/bootstrap/3.1.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
<script src="//netdna.bootstrapcdn.com/bootstrap/3.1.0/js/bootstrap.min.js"></script>
<script src="//code.jquery.com/jquery-1.11.1.min.js"></script>
<!------ Include the above in your HEAD tag ---------->

<div class="jumbotron jumbotron-sm">
    <div class="container">
        <div class="row">
            <div class="col-sm-12 col-lg-12">
                <h1 class="h1">
                    
            </div>
        </div>
    </div>
</div>

<div class="container">
    <div class="row">
        <div class="col-md-8">
            <div class="well well-sm">
                <form action="" method="POST">
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="name">
                                Adı</label>
                            <input type="text" class="form-control" name="adi" id="name" placeholder="Enter name" required="required" />
                        </div>
                        <div class="form-group">
                            <label for="email">
                                Email Adresi</label>
                            <div class="input-group">
                                <span class="input-group-addon"><span class="glyphicon glyphicon-envelope"></span>
                                </span>
                                <input type="email" class="form-control" id="email" name="email" placeholder="Enter email" required="required" /></div>
                        </div>
                        
                        <div class="form-group">
                            <label for="subject">
                                Kategori</label>
                            <select id="subject" name="kategori" class="form-control" required="required">
                                <option value="yazilim" selected="">yazılım:</option>
                                <option value="Donanım">Donanım</option>
                                <option value="SİStem">Sistem</option>
                                <option value="Ağ">Ağ</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="name">
                                Açıklama</label>
                            <textarea  id="message" name="aciklama" class="form-control" rows="9" cols="25" required="required"
                                placeholder="Message"></textarea>
                                
                        </div>
                    </div>
                    
                    <div class="col-md-12">
                    <h3><font color="red"><p id="test"></p></font><h3>
                        <button type="submit" class="btn btn-primary pull-right" id="btnContactUs">
                            Send Message</button>
                    </div>
                </div>
                </form>
            </div>
        </div>
        <div class="col-md-4">
            
        </div>
    </div>
</div>

<?php
if(isset($_POST['adi']))
{
    $query = $db->prepare("INSERT INTO ariza SET
adi = ?,
email = ?,
kategori = ?,
aciklama=?
");
$insert = $query->execute(array(
     $_POST['adi'],$_POST['email'],$_POST['kategori'],$_POST['aciklama']
));
if ( $insert ){
    $last_id = $db->lastInsertId();
    echo "
    <script>document.getElementById('test').innerHTML='Sorun bildirildi';</script>
    ";
}

}
?>