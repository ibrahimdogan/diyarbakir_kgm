<?php
include 'header.php';
?>
<!DOCTYPE html>
<html lang="en">
<head><script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
<script>
  (adsbygoogle = window.adsbygoogle || []).push({
    google_ad_client: "ca-pub-2286857915480120",
    enable_page_level_ads: true
  });
</script>
  <meta charset="utf-8">
  <title>Karayoları</title>
  <meta content="width=device-width, initial-scale=1.0" name="viewport">
  <meta content="" name="keywords">
  <meta content="" name="description">

  <!-- Favicons -->
  <link href="img/favicon.png" rel="icon">
  <link href="img/apple-touch-icon.png" rel="apple-touch-icon">

  <!-- Google Fonts -->
  <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,700,700i|Montserrat:300,400,500,700" rel="stylesheet">

  <!-- Bootstrap CSS File -->
  <link href="lib/bootstrap/css/bootstrap.min.css" rel="stylesheet">

  <!-- Libraries CSS Files -->
  <link href="lib/font-awesome/css/font-awesome.min.css" rel="stylesheet">
  <link href="lib/animate/animate.min.css" rel="stylesheet">
  <link href="lib/ionicons/css/ionicons.min.css" rel="stylesheet">
  <link href="lib/owlcarousel/assets/owl.carousel.min.css" rel="stylesheet">
  <link href="lib/lightbox/css/lightbox.min.css" rel="stylesheet">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
  <!-- Main Stylesheet File -->
  <link href="css/style.css" rel="stylesheet">

 <style>


 </style>
</head>

<body>

  <!--==========================
    Header
  ============================-->
  
<br><br><br>
<link href="//netdna.bootstrapcdn.com/bootstrap/3.0.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
<script src="//netdna.bootstrapcdn.com/bootstrap/3.0.0/js/bootstrap.min.js"></script>
<script src="//code.jquery.com/jquery-1.11.1.min.js"></script>
<!------ Include the above in your HEAD tag ---------->

<link href='https://fonts.googleapis.com/css?family=Roboto' rel='stylesheet'>
<style>

body {
    font-family: 'Roboto';font-size: 16px;
}

.aboutus-section {
    padding: 90px 0;
    margin-top:40px;
}
.aboutus-title {
    font-size: 30px;
    letter-spacing: 0;
    line-height: 32px;
    margin: 0 0 39px;
    padding: 0 0 11px;
    position: relative;
    text-transform: uppercase;
    color: #000;
}
.aboutus-title::after {
    background: #fdb801 none repeat scroll 0 0;
    bottom: 0;
    content: "";
    height: 2px;
    left: 0;
    position: absolute;
    width: 54px;
}
.aboutus-text {
    color: #606060;
    font-size: 13px;
    line-height: 22px;
    margin: 0 0 35px;
}

a:hover, a:active {
    color: #ffb901;
    text-decoration: none;
    outline: 0;
}
.aboutus-more {
    border: 1px solid #fdb801;
    border-radius: 25px;
    color: #fdb801;
    display: inline-block;
    font-size: 14px;
    font-weight: 700;
    letter-spacing: 0;
    padding: 7px 20px;
    text-transform: uppercase;
}
.feature .feature-box .iconset {
    background: #fff none repeat scroll 0 0;
    float: left;
    position: relative;
    width: 18%;
}
.feature .feature-box .iconset::after {
    background: #fdb801 none repeat scroll 0 0;
    content: "";
    height: 150%;
    left: 43%;
    position: absolute;
    top: 100%;
    width: 1px;
}

.feature .feature-box .feature-content h4 {
    color: #0f0f0f;
    font-size: 18px;
    letter-spacing: 0;
    line-height: 22px;
    margin: 0 0 5px;
}


.feature .feature-box .feature-content {
    float: left;
    padding-left: 28px;
    width: 78%;
}
.feature .feature-box .feature-content h4 {
    color: #0f0f0f;
    font-size: 18px;
    letter-spacing: 0;
    line-height: 22px;
    margin: 0 0 5px;
}
.feature .feature-box .feature-content p {
    color: #606060;
    font-size: 13px;
    line-height: 22px;
}
.icon {
    color : #f4b841;
    padding:0px;
    font-size:40px;
    border: 1px solid #fdb801;
    border-radius: 100px;
    color: #fdb801;
    font-size: 28px;
    height: 70px;
    line-height: 70px;
    text-align: center;
    width: 70px;
}
}
</style>
<div class="aboutus-section">
        <div class="container">
            <div class="row">
                <div class="col-md-6 col-sm-12 col-xs-12">
                    <div class="aboutus">
                        <h2 class="aboutus-title">HAKKIMIZDA</h2>
                        1923 yılında Türkiye Cumhuriyeti ilan edildiğinde; ulusal sınırlarımız içinde, 13.900 km'si  stabilize şose ve  4.450 km'si toprak olmak üzere,  toplam  18.350 km yol ve 94 köprü vardı.

 

Cumhuriyetin ilk yıllarında ulaşımda, dönemin en çağdaş teknolojisi olan demiryolu yapımı ağırlık kazandı, ancak bir süre sonra demiryolunun tek başına yeterli olmadığı, sistemin ucundaki ulaşım için karayoluna ihtiyaç olduğu görülerek, 1929 yılında Nafia Vekaleti (Bayındırlık Bakanlığı) içinde Şose ve Köprüler Reisliği kuruldu ve çıkarılan yol kanunu ile karayolu yapım çalışmalarına hız verildi.
<br>
         
                    </div>
                </div>
              
                <div class="col-md-5 col-sm-6 col-xs-12">
                    <div class="feature">
                        <div class="feature-box">
                            <div class="clearfix">
                                <div class="iconset">
                                    <span class="glyphicon glyphicon-cog icon"></span>
                                </div>
                                <div class="feature-content">
                                    <h4>Görevi ile ilgili işler için lüzumlu harita, etüd ve proje işlerini yapmak ve yaptırmak,

</h4>
                                    <p>

</p>
                                </div>
                            </div>
                        </div>
                        <div class="feature-box">
                            <div class="clearfix">
                                <div class="iconset">
                                    <span class="glyphicon glyphicon-cog icon"></span>
                                </div>
                                <div class="feature-content">
                                    <h4>Genel Müdürlüğün çalışmalarına ait bilgileri toplamak, basmak, yayınlamak,


</h4>

                                </div>
                            </div>
                        </div>
                        <div class="feature-box">
                            <div class="clearfix">
                                <div class="iconset">
                                    <span class="glyphicon glyphicon-cog icon"></span>
                                </div>
                                <div class="feature-content">
                                    <h4>Otoyol,Devlet ve İl yolları ile ilgili diğer kanunların tahmil ettiği işleri yapmak.

</h4>
                                    
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>



<!--==========================
    Footer
  ============================-->
  <footer id="footer">
    <div class="footer-top">
      <div class="container">
        <div class="row">

          <div class="col-lg-3 col-md-6 footer-info">
            <h3>Karayoları</h3>
            <p>
 ​Projelendirme, yapım, onarım, bakım ve diğer hususlar hakkında standartlar tespit etmek, teknik şartnameler hazırlamak,
 <br>
 Yolların kullanılması, yol ve trafik güvenliği ve bakımına ait esas ve kaideleri tespit etmek, yürütmek ve uygun göreceği yol işaretlerini tesis etmek, </p>
          </div>

          <div class="col-lg-3 col-md-6 footer-links">
            <h4>FAYDALI BAĞLANTILAR</h4>
            <ul>
              <li><i class="ion-ios-arrow-right"></i> <a href="/">Ana Sayfa</a></li>
              <li><i class="ion-ios-arrow-right"></i> <a href="/hakkimizda.php">Hakkımızda</a></li>
              <li><i class="ion-ios-arrow-right"></i> <a href="/iletisim.php">İletişim</a></li>
            </ul>
          </div>

          <div class="col-lg-3 col-md-6 footer-contact">
            <h4>İLETİŞİM</h4>
            <p>
            Yenişehir Mahallesi, Büyükalp <br>
                Cad., 21100 Yenişehir/Diyarbakır<br>
              <strong>Telefon:</strong> 312 - 449 84 92 <br>
              <strong>Faks:</strong> 312 - 449 84 92 <br>
              <strong>E-Posta:</strong> info@karayoları.com.tr<br>
            </p>

        

          </div>

          <div class="col-lg-3 col-md-6 footer-newsletter">
            <h4>BÜLTENİMİZ</h4>
            <p>Otoyol, Devlet ve İl yolları ağını tespit etmek ve bu ağdaki değişiklikleri hazırlamak,Yol ağı üzerindeki yol ve köprüleri inşa ve islah etmek,onarmak ve emniyetle kullanmalarını sağlayacak şekilde sürekli bakım altında bulundurmak ve bu konularda gerekli eğitim yapmak,
 </p>
            
          </div>

        </div>
      </div>
    </div>

    <div class="container">
      <div class="copyright">
        &copy; Copyright <strong>Karayoları</strong> Her Hakkı Saklıdır
      </div>
      <div class="credits">
        <!--
          All the links in the footer should remain intact.
          You can delete the links only if you purchased the pro version.
          Licensing information: https://bootstrapmade.com/license/
          Purchase the pro version with working PHP/AJAX contact form: https://bootstrapmade.com/buy/?theme=BizPage
        -->
       
      </div>
    </div>
  </footer><!-- #footer -->


  <a href="#" class="back-to-top"><i class="fa fa-chevron-up" style="margin-left:-15px; margin-top:-10px;"></i></a>

  <!-- JavaScript Libraries -->
  <script src="lib/jquery/jquery.min.js"></script>
  <script src="lib/jquery/jquery-migrate.min.js"></script>
  <script src="lib/bootstrap/js/bootstrap.bundle.min.js"></script>
  <script src="lib/easing/easing.min.js"></script>
  <script src="lib/superfish/hoverIntent.js"></script>
  <script src="lib/superfish/superfish.min.js"></script>
  <script src="lib/wow/wow.min.js"></script>
  <script src="lib/waypoints/waypoints.min.js"></script>
  <script src="lib/counterup/counterup.min.js"></script>
  <script src="lib/owlcarousel/owl.carousel.min.js"></script>
  <script src="lib/isotope/isotope.pkgd.min.js"></script>
  <script src="lib/lightbox/js/lightbox.min.js"></script>
  <script src="lib/touchSwipe/jquery.touchSwipe.min.js"></script>
  <!-- Contact Form JavaScript File -->
  <script src="contactform/contactform.js"></script>

  <!-- Template Main Javascript File -->
  <script src="js/main.js"></script>

</body>
</html>
