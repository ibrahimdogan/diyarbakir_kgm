<?php
session_start();
if(!(isset($_SESSION['kullanici_adi'])))
{
Header('Location:giris.php');
}
include 'baglanti.php';
include 'header.php';
?>
<link href="//netdna.bootstrapcdn.com/bootstrap/3.0.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
<script src="//netdna.bootstrapcdn.com/bootstrap/3.0.0/js/bootstrap.min.js"></script>
<script src="//code.jquery.com/jquery-1.11.1.min.js"></script>
<!------ Include the above in your HEAD tag ---------->
<style>
.bos{
    width:25px;
    height:150px;
    
}
</style>
<div class="bos">

</div>
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="alert alert-info">
            Gelen mesaj ve Öneriler</div>
            <div class="alert alert-success" style="display:none;">
                <span class="glyphicon glyphicon-ok"></span> Gelen mesaj ve Öneriler</div>
            <table class="table">
                <thead>
                    <tr>
                        <th class="active">
                            ADI
                        </th >
                        <th class="success">
                          E-POSTA
                        </th>
                        <th class="warning">
                            KONU
                        </th>
                        <th class="danger">
                            AÇIKLAMA
                        </th>
                    </tr>
                </thead>
                <tbody>
            <?php
            $query = $db->query("SELECT * FROM mesaj", PDO::FETCH_ASSOC);
            if ( $query->rowCount() ){
                 foreach( $query as $row ){
                  
            echo "
                    <tr>
                        <td class='active'>".
                         $row['adi']
                        ."
                        </td>
                        <td class='success'>
                        ".
                        $row['email']
                       ."
                        </td>
                        <td th class='warning'>
                        ".
                        $row['konu']
                       ."
                        </td>
                        <td class='danger'>
                        ".
                        $row['aciklama']
                       ."
                        </td>
                    </tr>
                    ";

                }
            }
                ?>    
                    
                </tbody>
            </table>
        </div>
    </div>
</div>
